// ==UserScript==
// @name         Arbeitszeitanzeiger
// @namespace    https://jloewe.net/
// @version      0.4
// @description  Zeigt die Arbeitszeit in ZEUS an
// @author       Jan Henry Loewe
// @match        https://p-zeus.bs.ptb.de/*/Today.aspx
// @match        https://ppp.ptb.de/*/Today.aspx
// @grant        GM_setValue
// @grant        GM_getValue
// @require      https://code.jquery.com/jquery-3.4.1.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/de.js
// @updateURL	 https://gitlab.com/jloewe/arbeitszeitanzeiger/raw/master/arbeitszeitanzeiger.user.js
// ==/UserScript==

const m = moment;
m.locale("de")

const $ = window.$;

const defaultHours = getMinutesForDay(parseInt(m().format("d")));
const step = 15;
const minMinutes = 1 * 60;
const maxMinutes = 10 * 60;

(function() {
    'use strict';

    // add report iframe
    let frame = $(`<iframe style="display: none" src="${window.location.href.replace("Today.aspx", "Module/Report/Report.aspx")}"></iframe>`);

    frame.appendTo("body");

    let interval = setInterval(() => {
        // extract report from iframe
        let report = frame.contents().find("#FrmReportContent").contents().find("#uiReport").text();

        if(report.length > 0) {
            // report successfully loaded
            clearInterval(interval);

            let t = m();

            let rawRegex = /^.{3} ?(?<dayOfTheMonth>\d?\d)  ?(?<TM>\d?\d\d\d) \|((?<TE>\d\d\d)|   ) ((?<FZ>\S\S\S)|   )    ?(?<startTime>\d?\d:\d\d) - ( ?(?<endTime>\d?\d:\d\d))?/gm;
            console.log(`Using RegEx: ${rawRegex}`);
            let matches = [];
            let match;
            while ((match = rawRegex.exec(report)) != null) {
                matches.push(match.groups);
            }

            console.log("Matches: ", matches);

            if(matches && matches.length) {
                let todaysMatches = matches.filter(x => x.dayOfTheMonth === t.format("D"));
                let totalDiff = m.duration(0);

                for(let dayPart of todaysMatches) {
                    if(dayPart.startTime) {
                        if(dayPart.endTime) {
                            let diff = m.duration(m(dayPart.startTime, "HH:mm").diff(m(dayPart.endTime, "HH:mm")));
                            // diff is negative but we want a positive total diff
                            totalDiff.subtract(diff);
                        } else {
                            let diff = m.duration(m(dayPart.startTime, "HH:mm").diff(m()));
                            // diff is negative but we want a positive total diff
                            totalDiff.subtract(diff);
                        }
                    }
                }
                console.log(`Arbeitszeit ohne Pause: ${Math.abs(totalDiff.hours())}h ${Math.abs(totalDiff.minutes())}min`);

                if(todaysMatches.length && todaysMatches[0].startTime) {
                    let firstBooking = m(todaysMatches[0].startTime, "HH:mm");

                    $("#firstBooking").text(firstBooking.format("H:mm [Uhr]"));

                    let range = $("#hourPicker");

                    range.on("input", function(){
                        setLeaveTime(totalDiff.clone(), range.val());
                    });

                    setLeaveTime(totalDiff.clone(), range.val());

                    $("#setToCurrentTimeBtn").on("click", function (e) {
                        e.preventDefault();
                        range.val(totalDiff.asMinutes());
                        setLeaveTime(totalDiff.clone(), Math.round(totalDiff.asMinutes()));
                    });
                }
            } else {
                $("#error").text("ERROR: Erste Buchung konnte nicht geladen werden.").show();
            }
        }
    }, 400);

    addUi();
})();

function addUi() {
    $(`
<fieldset class="fieldset_base">
<legend class="base blue bold">
Arbeitszeitanzeiger
</legend>

<div style="padding:0 10px 10px 10px; max-width: 250px; margin-left: auto; margin-right: auto;" id="arbeitszeitanzeigerTab">
<div id="error" style="
    display: none;
    padding: 10px;
    color: red;
    border: 1px solid red;
    margin: 10px 0 10px;
"></div>
<div style="width: 100%;">
<input style="width: 100%;" type="range" id="hourPicker" value="${defaultHours}" min="${minMinutes}" max="${maxMinutes}" step="${step}">
<span>${minMinutes / 60}h</span>
<span style="float: right;">${maxMinutes / 60}h</span>
<div style="clear: right;"></div>
</div>

<table style="margin-top: 20px;">
<tbody>
<tr><td class="base blue bold">Erste Buchung:</td><td id="firstBooking" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Arbeitszeit:</td><td id="duration" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Auswirkung auf Saldo:</td><td id="diffToDefault" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Enthaltene Pausenzeit:</td><td id="breakTime" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Feierabend:</td><td id="endTime" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Verbleibend:</td><td id="remainingTime" class="base blue bold"></td></tr>
</tbody>
</table>
<div style="text-align: center; margin-top: 20px;">
<button id="setToCurrentTimeBtn">Auf aktuelle Zeit setzen</button>
</div>
</div>

<div style="display: none; padding:0 10px 10px 10px; max-width: 250px; margin-left: auto; margin-right: auto;" id="settingsTab">
<p>Arbeitszeiten müssen in Minuten angegeben werden!</p>

<table style="margin-top: 20px">
<tbody>
<tr><td class="base blue bold"><label for="mondayMinutes">Montag</label></td><td class="base blue bold"><input id="mondayMinutes" name="mondayMinutes" value="${getMinutesForDay(1)}"></td></tr>
<tr><td class="base blue bold"><label for="tuesdayMinutes">Dienstag</label></td><td class="base blue bold"><input id="tuesdayMinutes" name="tuesdayMinutes" value="${getMinutesForDay(2)}"></td></tr>
<tr><td class="base blue bold"><label for="wednesdayMinutes">Mittwoch</label></td><td class="base blue bold"><input id="wednesdayMinutes" name="wednesdayMinutes" value="${getMinutesForDay(3)}"></td></tr>
<tr><td class="base blue bold"><label for="thursdayMinutes">Donnerstag</label></td><td class="base blue bold"><input id="thursdayMinutes" name="thursdayMinutes" value="${getMinutesForDay(4)}"></td></tr>
<tr><td class="base blue bold"><label for="fridayMinutes">Freitag</label></td><td class="base blue bold"><input id="fridayMinutes" name="fridayMinutes" value="${getMinutesForDay(5)}"></td></tr>
</tbody>
</table>

<button id="settingsSaveBtn">Speichern</button>
</div>

<div id="navigationBtn" style="text-align: center; cursor: pointer;">
<p>Edit Settings</p>
<p style="display: none;">Go Back</p>
</div>
</fieldset>`).appendTo("#uiDivMyAccounts");

    $("#navigationBtn").on("click", function () {
        $("#navigationBtn > p, #arbeitszeitanzeigerTab, #settingsTab").toggle();
    });

    $("#settingsSaveBtn").on("click", function () {
        GM_setValue("mondayMinutes", $("#mondayMinutes").val());
        GM_setValue("tuesdayMinutes", $("#tuesdayMinutes").val());
        GM_setValue("wednesdayMinutes", $("#wednesdayMinutes").val());
        GM_setValue("thursdayMinutes", $("#thursdayMinutes").val());
        GM_setValue("fridayMinutes", $("#fridayMinutes").val());
    });
}

function setLeaveTime(workingDuration, duration) {
    let remainingDuration = m.duration(parseInt(duration), "minutes");

    // add break time to remainingDuration
    let breakTime = getBreakTime(duration);
    remainingDuration.add(breakTime, "minutes");

    // subtract already workingDuration from remainingDuration
    remainingDuration.subtract(workingDuration);
    console.log(`Verbleibend: ${Math.abs(remainingDuration.hours())}h ${Math.abs(remainingDuration.minutes())}min`);

    const endTime = m().add(remainingDuration);

    $("#duration").text(`${Math.floor(duration / 60)}h ${duration % 60 > 0 ? `${duration % 60}min` : ""}`);
    $("#breakTime").text(`${breakTime}min`);

    let diffToDefault = duration - defaultHours;
    let diffToDefaultDuration = m.duration(Math.abs(diffToDefault), "minutes");
    $("#diffToDefault").text(`${diffToDefault < 0 ? "-" : "+"}${diffToDefaultDuration.hours()}h ${diffToDefaultDuration.minutes()}min`);

    $("#endTime").text(endTime.format("H:mm [Uhr]"));
    $("#remainingTime").text(`${remainingDuration.asSeconds() < 0 ? "-" : "+"}${Math.abs(remainingDuration.hours())}h ${Math.abs(remainingDuration.minutes())}min`);
}

function getMinutesForDay(dayIndex) {
    switch (dayIndex) {
            // monday
        case 1:
            return GM_getValue("mondayMinutes", 8 * 60);
            // tuesday
        case 2:
            return GM_getValue("tuesdayMinutes", 8 * 60 + 15);
            // wednesday
        case 3:
            return GM_getValue("wednesdayMinutes", 8 * 60 + 15);
            // thursday
        case 4:
            return GM_getValue("thursdayMinutes", 8 * 60 + 15);
            // friday
        case 5:
            return GM_getValue("fridayMinutes", 6 * 60 + 15);
        default:
            throw "Invalid day index";
    }
}

function getBreakTime(duration) {
    // 0 minute break time for less than 6 hours
    if (duration <= 6 * 60) return 0;

    // 35 minute break time for 6 to 9 hours
    if (duration <= 9 * 60) return 30;

    // 45 minute break time for more than 9 hours
    return 45;
}
